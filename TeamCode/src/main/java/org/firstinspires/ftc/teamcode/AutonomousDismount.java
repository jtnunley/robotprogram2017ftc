package org.firstinspires.ftc.teamcode;

import android.media.MediaPlayer;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcontroller.internal.FtcRobotControllerActivity;

import java.lang.annotation.ElementType;

@Autonomous(name="Dismount From Shuttle =uwu=")
public final class AutonomousDismount extends AutonomousOpMode {
    @Override
    public void initialize() {
        Robot.GetLiftMotor().runMotorsWithoutEncoders(); Robot.GetChainMotor().runMotorsWithoutEncoders();
    }

    @Override
    public boolean runProgram() {
        /*DcMotor[] motors = {lMotor1,lMotor2,rMotor1,rMotor2};
        double[] speed = {5*12,5*12,5*12,5*12};
        AutonomousHelper.encoderDrive(0.5,speed,25.0,motors,this);*/

        Robot.GetLiftMotor().setMotorPower(-1.0);
        Robot.GetChainMotor().setMotorPower(-0.46);

        ElapsedTime et = new ElapsedTime();
        while (et.milliseconds() <= 12000 && opModeIsActive());

        Robot.GetLiftMotor().setMotorPower(0.0);
        Robot.GetChainMotor().setMotorPower(0.0);

        return false;
    }
}
