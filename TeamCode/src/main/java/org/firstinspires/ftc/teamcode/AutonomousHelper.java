package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import static java.lang.Math.abs;

public final class AutonomousHelper {
    static final double     COUNTS_PER_MOTOR_REV    = 1440 ;    // eg: TETRIX Motor Encoder
    static final double     DRIVE_GEAR_REDUCTION    = 2.0 ;     // This is < 1.0 if geared UP
    static final double     WHEEL_DIAMETER_INCHES   = 4.0 ;     // For figuring circumference
    static final double     COUNTS_PER_INCH         = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) /
            (WHEEL_DIAMETER_INCHES * 3.1415);
    static final double     DRIVE_SPEED             = 0.6;
    static final double     TURN_SPEED              = 0.5;

    private static ElapsedTime runtime = new ElapsedTime();
    public static void prepareForEncoderDrive(Hardware Robot) {
        Robot.GetDrivetrain().resetMotorEncoders();
        Robot.GetDrivetrain().runMotorsWithEncoders();
    }

    // gelper function
    public static boolean areAnyBusy(SingleMotor[] motors) {
        for (int i = 0; i < motors.length; i++) {
            if (motors[i].isBusy()) return true;
        }
        return false;
    }

    public static void encoderDrive(double speed,
                                    double[] inches,
                                    double timeoutS, SingleMotor[] motors, LinearOpMode opMode) {
        int[] newTargets = new int[motors.length];

        // Ensure that the opmode is still active
        if (opMode.opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            for (int i = 0; i < motors.length; i++) {
                motors[i].setTarget(motors[i].getPosition() + (int)(inches[i] + COUNTS_PER_INCH));
            }

            // Turn On RUN_TO_POSITION
            for (int i = 0; i < motors.length; i++) {
                motors[i].setRunToPosition();
                motors[i].setMotorPower(abs(speed));
            }
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opMode.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    areAnyBusy(motors)) {

            }

            for (int i = 0; i < motors.length; i++) {
                motors[i].setMotorPower(0.0);
                motors[i].runMotorsWithEncoders();
            }

            //  sleep(250);   // optional pause after each move
        }
    }

    public static void encoderDrive(double speed,
                                    double[] inches,
                                    double timeoutS, Drivetrain dr, LinearOpMode opMode) {
        SingleMotor motors[] = {dr.lDrive,dr.rDrive};
        encoderDrive(speed,inches,timeoutS,motors,opMode);
    }

    public static void encoderDrivetrainTurn(double speed, double inches, boolean clockwise, double timeout, Drivetrain dr, LinearOpMode opm) {
        double inch1,inch2;
        if (clockwise) { inch1 = inches; inch2 = -inches; }
        else { inch1 = -inches; inch2 = inches; }
        double trueInches[] = {inch1,inch2};
        encoderDrive(speed,trueInches,timeout,dr,opm);
    }
}
