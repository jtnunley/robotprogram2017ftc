package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="TELEMETRY TESTING")
public final class telemetryTest extends LinearOpMode {
    DcMotor dm;

    @Override
    public void runOpMode() {
        dm = hardwareMap.dcMotor.get("lDrive");
        dm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        waitForStart();
        ElapsedTime et = new ElapsedTime();
        while (et.milliseconds() < 5000 && this.opModeIsActive()) {
            telemetry.clear();
            telemetry.addData("Motor Power",dm.getPower());
        }

        dm.setPower(1.0);
        et.reset();
        while (et.milliseconds() < 10000 && this.opModeIsActive()) {
            telemetry.clear();
            telemetry.addData("Motor Power",dm.getPower());
        }

        dm.setPower(0.0);
        et.reset();
        while (et.milliseconds() < 15000 && this.opModeIsActive()) {
            telemetry.clear();
            telemetry.addData("Motor Power",dm.getPower());
        }
    }
}
