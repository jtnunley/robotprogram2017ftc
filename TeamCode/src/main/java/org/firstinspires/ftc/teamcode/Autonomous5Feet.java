package org.firstinspires.ftc.teamcode;

import android.media.MediaPlayer;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcontroller.internal.FtcRobotControllerActivity;

import java.lang.annotation.ElementType;

@Autonomous(name="5 Seconds Forwards")
public final class Autonomous5Feet extends AutonomousOpMode {
    @Override
    public void initialize() {
        Robot.GetDrivetrain().runMotorsWithoutEncoders();
    }

    @Override
    public boolean runProgram() {
        /*DcMotor[] motors = {lMotor1,lMotor2,rMotor1,rMotor2};
        double[] speed = {5*12,5*12,5*12,5*12};
        AutonomousHelper.encoderDrive(0.5,speed,25.0,motors,this);*/

        Robot.GetDrivetrain().setMotorPower(0.5);

        ElapsedTime et = new ElapsedTime();
        while (et.milliseconds() <= 5000 && opModeIsActive());

        Robot.GetDrivetrain().setMotorPower(0.0);

        return false;
    }
}
