package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name="3 Feet Forwards")
public class AutonomousEncoderDrive extends AutonomousOpMode {
    @Override
    public void initialize() {
        Robot.GetDrivetrain().resetMotorEncoders();
        Robot.GetDrivetrain().runMotorsWithEncoders();
    }

    @Override
    public boolean runProgram() {
        runHook();
        double inches[] = {3*12,3*12};
        AutonomousHelper.encoderDrive(0.5,inches,20,Robot.GetDrivetrain(),this);
        return false;
    }
}
