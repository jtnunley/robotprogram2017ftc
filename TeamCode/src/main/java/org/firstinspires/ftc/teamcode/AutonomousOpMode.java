package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

public abstract class AutonomousOpMode extends OpModeBase {

    public void runHook() {
        if (isRunningHook()) {
            Robot.GetLiftMotor().setMotorPower(-1.0);
            Robot.GetChainMotor().setMotorPower(-0.46);

            ElapsedTime et = new ElapsedTime();
            while (et.milliseconds() <= 12000 && opModeIsActive());

            Robot.GetLiftMotor().setMotorPower(0.0);
            Robot.GetChainMotor().setMotorPower(0.0);
        }
    }

    public boolean isRunningHook() { return false; }

    @Override
    public boolean isAutonomous() {
        return true;
    }
}
