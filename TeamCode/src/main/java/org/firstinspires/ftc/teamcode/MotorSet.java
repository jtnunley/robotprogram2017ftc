package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import java.util.LinkedList;

public class MotorSet implements MotorController {

    public LinkedList<DcMotor> motors = new LinkedList<DcMotor>();
    public int motorCount;

    public MotorSet(String motorName, int motorCount, HardwareMap hwx) {
        for (int i = 1; i <= motorCount; i++) {
            motors.add(hwx.dcMotor.get(motorName + i));
            motors.get(i - 1).setDirection(DcMotorSimple.Direction.FORWARD);
        }
    }

    public MotorSet(LinkedList<DcMotor> motors) { this.motors = motors; }

    public MotorSet() {  }


    @Override
    public double getValue(int index) {
        return motors.get(index).getPower();
    }

    public void reverseLeftMotor() { for (int i = 0; i < motorCount; i++) motors.get(i).setDirection(DcMotorSimple.Direction.REVERSE); }

    // set power for one/both motors
    public void setMotorPower(double power) {
        for (int i = 0; i < motorCount; i++) motors.get(i).setPower(power);
    }

    // set both motors to run without encoders
    public void runMotorsWithoutEncoders() {
        for (int i = 0; i < motorCount; i++) motors.get(i).setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    // reset one/both of motor's encoders
    public void resetMotorEncoders() {
        for (int i = 0; i < motorCount; i++) motors.get(i).setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    // run one/both motors with encoders
    public void runMotorsWithEncoders() {
        for (int i = 0; i < motorCount; i++) motors.get(i).setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public int getPosition() { return motors.get(0).getCurrentPosition(); }

    public void setTarget(int target) { for (int i = 0; i < motorCount; i++) motors.get(i).setTargetPosition(target); }

    public void setRunToPosition() { for (int i = 0; i < motorCount; i++) motors.get(i).setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER); }

    public boolean isBusy() { return motors.get(0).isBusy(); }
}
