package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name="45 Degree Turn (Hook)")
public final class Autonomous45TurnHook extends Autonomous45Turn {
    @Override
    public boolean isRunningHook() {
        return true;
    }
}
