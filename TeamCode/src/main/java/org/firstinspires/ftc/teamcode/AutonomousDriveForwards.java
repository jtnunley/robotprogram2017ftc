package org.firstinspires.ftc.teamcode;

import android.text.style.TtsSpan;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="Drive Forwards")
public final class AutonomousDriveForwards extends AutonomousOpMode {

    @Override
    public void initialize() {
        Robot.GetDrivetrain().runMotorsWithoutEncoders();
    }

    @Override
    public boolean runProgram() {
        ElapsedTime et = new ElapsedTime();
        Robot.GetDrivetrain().setMotorPower(1.0);

        while (et.milliseconds() <= 5000 && opModeIsActive());
        Robot.GetDrivetrain().setMotorPower(0.0);

        return false;
    }
}
