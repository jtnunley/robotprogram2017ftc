package org.firstinspires.ftc.teamcode;

import android.graphics.Region;
import android.media.MediaPlayer;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public abstract class OpModeBase extends LinearOpMode {
    public abstract boolean isAutonomous();
    public abstract void initialize();
    public abstract boolean runProgram();

    protected MediaPlayer mp;

    private void realinit() {
        Robot = new Hardware(hardwareMap);

        initialize();
    }

    protected Hardware Robot;


    public OpModeBase() {

    }

    @Override
    public void runOpMode() throws InterruptedException {
        realinit();
        waitForStart();
        TelemetryManager.TelemetryManagerTask tmt = new TelemetryManager.TelemetryManagerTask(Robot,this.telemetry,this);
        Thread thread = new Thread(tmt);
        thread.run();

        if (isAutonomous()) runProgram();
        else while (opModeIsActive() && runProgram()) {
        }

        if (mp != null) mp.stop();
    }
}
