package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class SingleMotor implements MotorController {

    public double multiplier = 1.0;

    public DcMotor motor;

    public SingleMotor(String name, HardwareMap hmx) {
        motor = hmx.dcMotor.get(name);
    }

    @Override
    public void setMotorPower(double power) {
        motor.setPower(power * multiplier);
    }

    @Override
    public void runMotorsWithoutEncoders() {
        motor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    @Override
    public void resetMotorEncoders() {
        motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    @Override
    public void runMotorsWithEncoders() {
        motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    @Override
    public int getPosition() {
        return motor.getCurrentPosition();
    }

    @Override
    public void setTarget(int target) {
        motor.setTargetPosition(target);
    }

    @Override
    public void setRunToPosition() {
        motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    @Override
    public boolean isBusy() {
        return motor.isBusy();
    }

    @Override
    public double getValue(int index) {
        return 0;
    }

    public void reverseMotor() {
        motor.setDirection(DcMotorSimple.Direction.REVERSE);
    }
}
