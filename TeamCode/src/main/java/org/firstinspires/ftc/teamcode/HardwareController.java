package org.firstinspires.ftc.teamcode;

public interface HardwareController {
    double getValue(int index);
}
