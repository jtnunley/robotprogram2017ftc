package org.firstinspires.ftc.teamcode;

import android.app.backup.RestoreObserver;
import android.media.MediaPlayer;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcontroller.internal.FtcRobotControllerActivity;

@TeleOp(name="TeleOp")
public final class TankTeleOp extends TeleOpMode {


    @Override
    public void initialize() {

        Robot.GetDrivetrain().runMotorsWithoutEncoders();

        //mp = MediaPlayer.create(FtcRobotControllerActivity.theContext(),R.raw.minecraft_parody);
        //mp.start();
    }

    // main program loop - runs until program is shut down
    @Override
    public boolean runProgram() {
        double left,right,drive,turn,max;
        // all code made by paco, nobody else, paco made this, thank you paco, very cool!

        // drive (vroom vroom forwards movement) and turn (sideways movement)
        turn = -gamepad1.left_stick_x;
        drive  =  gamepad1.left_stick_y;

        // Combine drive and turn for blended motion.
        left  = drive + turn;
        right = drive - turn;

        // Normalize the values so neither exceed +/- 1.0
        max = Math.max(Math.abs(left), Math.abs(right));
        if (max > 1.0)
        {
            left /= max;
            right /= max;
        }

        //Robot.GetDrivetrain().setMotorPower(left,right);

        // set multipliers for variables
        double scaling = 1; //0.5 //BIG AND REALLY COOL
        if (gamepad1.y) scaling = 0.99;
        // if (gamepad1.a) mp.stop();

        Robot.GetDrivetrain().lDrive.multiplier = scaling;
        Robot.GetDrivetrain().rDrive.multiplier = scaling;

        Robot.GetDrivetrain().setMotorPower(left,right);
        /*
        double power = 0.0;
        if (gamepad1.left_bumper) power += 0.5;
        if (gamepad1.right_bumper) power -= 0.5;
        Robot.GetLiftMotor().setMotorPower(power);
        */

        // we use weird values for these
        if (gamepad1.x) {
            Robot.GetLiftMotor().setMotorPower(0.1);
            Robot.GetChainMotor().setMotorPower(1.0);
        }
        else if (gamepad1.b) {
            Robot.GetLiftMotor().setMotorPower(-0.25);
            Robot.GetChainMotor().setMotorPower(-1.0);
        }
        else {
            Robot.GetLiftMotor().setMotorPower(0.0);
            Robot.GetChainMotor().setMotorPower(0.0);
        }


        return true;
    }
}
