package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;

public final class Hardware {
    private HardwareMap hmx;

    private Drivetrain drivetrain;
    public Drivetrain GetDrivetrain() {
        if (drivetrain == null) drivetrain = new Drivetrain(hmx);
        return drivetrain;
    }

    private SingleMotor liftMotor;
    public SingleMotor GetLiftMotor() {
        if (liftMotor == null)  {
            liftMotor = new SingleMotor(Constants.liftMotorName,hmx);
            liftMotor.multiplier = -1.0;
        }
        return liftMotor;
    }

    private SingleMotor chainMotor;
    public SingleMotor GetChainMotor() {
        if (chainMotor == null) chainMotor = new SingleMotor(Constants.chainMotorName,hmx);
        return chainMotor;
    }

    public Hardware(HardwareMap hm) {
        hmx = hm;
    }
}
