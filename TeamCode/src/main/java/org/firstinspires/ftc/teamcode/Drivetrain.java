package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;

public class Drivetrain implements MotorController {
    //private DcMotor lDrive1,lDrive2,rDrive1,rDrive2;
    public SingleMotor lDrive,rDrive;

    @Override
    public int getPosition() {
        return lDrive.getPosition();
    }

    @Override
    public void setTarget(int target) {
        setLeftTarget(target); setRightTarget(target);
    }

    @Override
    public boolean isBusy() {
        return false;
    }

    @Override
    public double getValue(int index) {
        return 0;
    }

    private static final String lDriveName = "lDrive";
    private static final String rDriveName = "rDrive";

    public Drivetrain(HardwareMap hwMap) {


        lDrive = new SingleMotor(Constants.lDriveName,hwMap);
        rDrive = new SingleMotor(Constants.rDriveName,hwMap);

        lDrive.multiplier = Constants.lMotorRate;
        rDrive.multiplier = Constants.rMotorRate;

        setMotorPower(0);

        reverseLeftMotor();

        runMotorsWithoutEncoders();
    }

    // set motors to reverse
    public void reverseLeftMotor() {
        lDrive.reverseMotor();
    }
    public void reverseRightMotor() {
        rDrive.reverseMotor();
    }

    // set power for one/both motors
    public void setLeftMotorPower(double power) { lDrive.setMotorPower(power);}
    public void setRightMotorPower(double power) { rDrive.setMotorPower(power); }
    public void setMotorPower(double left, double right) { setLeftMotorPower(left); setRightMotorPower(right);}
    public void setMotorPower(double power) {
        setMotorPower(power, power);
    }


    // set both motors to run without encoders
    public void runLeftMotorWithoutEncoders() { lDrive.runMotorsWithoutEncoders();}
    public void runRightMotorWithoutEncoders() { rDrive.runMotorsWithEncoders();}
    public void runMotorsWithoutEncoders() { runLeftMotorWithoutEncoders();runRightMotorWithoutEncoders();}

    // reset one/both of motor's encoders
    public void resetLeftMotorEncoders() { lDrive.resetMotorEncoders();}
    public void resetRightMotorEncoders() { rDrive.resetMotorEncoders();}
    public void resetMotorEncoders() { resetLeftMotorEncoders();resetRightMotorEncoders();}

    // run one/both motors with encoders
    public void runLeftMotorWithEncoders() { lDrive.runMotorsWithEncoders();}
    public void runRightMotorWithEncoders() { rDrive.runMotorsWithEncoders();}
    public void runMotorsWithEncoders() { runLeftMotorWithEncoders();runRightMotorWithEncoders();}

    public int getLeftPosition() { return lDrive.getPosition();}
    public int getRightPosition() { return rDrive.getPosition();}

    public void setLeftTarget(int target) { lDrive.setTarget(target);}
    public void setRightTarget(int target) { rDrive.setTarget(target);}

    public void setLeftToRunToPosition() { lDrive.setRunToPosition(); }
    public void setRightToRunToPosition() { rDrive.setRunToPosition(); }
    public void setRunToPosition() { setLeftToRunToPosition();setRightToRunToPosition();}

    public boolean isLeftBusy() { return lDrive.isBusy(); }
    public boolean isRightBusy() { return rDrive.isBusy(); }

}

/*
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import java.util.LinkedList;

public class Drivetrain extends MotorSet {

    @Override
    public double getValue(int index) {
        return 0;
    }

    private static final String lDriveName = "lDrive";
    private static final String rDriveName = "rDrive";

    public MotorSet leftMotors,rightMotors;

    public Drivetrain(HardwareMap hwMap) {
        leftMotors = new MotorSet(Constants.lDriveName,Constants.lDriveCount,hwMap);
        rightMotors = new MotorSet(Constants.rDriveName,Constants.rDriveCount,hwMap);

        this.motors.addAll(leftMotors.motors);
        this.motors.addAll(rightMotors.motors);

        setMotorPower(0);

        runMotorsWithoutEncoders();
    }

    int motorCount() { return 2; }

    // set motors to reverse
    public void reverseLeftMotor() { leftMotors.reverseLeftMotor();}
    public void reverseRightMotor() { rightMotors.reverseLeftMotor();}

    // set power for one/both motors
    public void setLeftMotorPower(double power) { leftMotors.setMotorPower(power);}
    public void setRightMotorPower(double power) { rightMotors.setMotorPower(power);}
    public void setMotorPower(double lpower, double rpower) { setLeftMotorPower(lpower);setMotorPower(rpower);}


    // set both motors to run without encoders
    public void runLeftMotorWithoutEncoders() { leftMotors.runMotorsWithoutEncoders();}
    public void runRightMotorWithoutEncoders() { rightMotors.runMotorsWithoutEncoders();}

    // reset one/both of motor's encoders
    public void resetLeftMotorEncoders() { leftMotors.resetMotorEncoders();}
    public void resetRightMotorEncoders() { rightMotors.resetMotorEncoders();}

    // run one/both motors with encoders
    public void runLeftMotorWithEncoders() { leftMotors.runMotorsWithEncoders();}
    public void runRightMotorWithEncoders() { rightMotors.runMotorsWithEncoders();}

    public int getLeftPosition() { return leftMotors.getPosition();}
    public int getRightPosition() { return rightMotors.getPosition();}

    public void setLeftTarget(int target) { leftMotors.setTarget(target);}
    public void setRightTarget(int target) { rightMotors.setTarget(target);}

    public void setLeftToRunToPosition() { leftMotors.setRunToPosition(); }
    public void setRightToRunToPosition() { rightMotors.setRunToPosition(); }

    public boolean isLeftBusy() { return leftMotors.isBusy(); }
    public boolean isRightBusy() { return rightMotors.isBusy(); }

}
*/
