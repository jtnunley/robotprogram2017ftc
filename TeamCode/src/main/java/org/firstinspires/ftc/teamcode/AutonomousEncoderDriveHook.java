package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name="3 Feet Forwards (Hook)")
public final class AutonomousEncoderDriveHook extends AutonomousEncoderDrive {
    @Override
    public boolean isRunningHook() {
        return true;
    }
}
