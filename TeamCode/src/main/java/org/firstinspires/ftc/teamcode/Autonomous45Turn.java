package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name="45 Degree Turn")
public class Autonomous45Turn extends AutonomousOpMode {
    @Override
    public void initialize() {
        Robot.GetDrivetrain().resetMotorEncoders();
        Robot.GetDrivetrain().runMotorsWithEncoders();
    }

    @Override
    public boolean runProgram() {
        runHook();
        double inches[] = {36,36};
        AutonomousHelper.encoderDrive(0.5,inches,20,Robot.GetDrivetrain(),this);
        AutonomousHelper.encoderDrivetrainTurn(0.5,12,false,20,Robot.GetDrivetrain(),this);
        double inches2[] = {72,72};
        AutonomousHelper.encoderDrive(0.5,inches2,20,Robot.GetDrivetrain(),this);
        return false;
    }
}