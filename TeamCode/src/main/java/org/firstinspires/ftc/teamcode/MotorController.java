package org.firstinspires.ftc.teamcode;

public interface MotorController extends HardwareController {
    void setMotorPower(double power);
    void runMotorsWithoutEncoders();
    void resetMotorEncoders();
    void runMotorsWithEncoders();
    int getPosition();
    void setTarget(int target);
    void setRunToPosition();
    boolean isBusy();
}
