package org.firstinspires.ftc.teamcode;

public final class Constants {
    public static final String lDriveName = "lDrive";
    public static final String rDriveName = "rDrive";
    public static final String liftMotorName = "lMotor";
    public static final String chainMotorName = "cMotor";
    public static final int lDriveCount = 2;
    public static final int rDriveCount = 2;

    public static final double lMotorRate = 1; //0.5
    public static final double rMotorRate = 1; //0.5

}
