package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public final class TelemetryManager {
    public static void manageTelem(Hardware h, Telemetry telemetry) {
        telemetry.clear();
        telemetry.addData("Left Motor: ",h.GetDrivetrain().lDrive.motor.getPower());
        telemetry.addData("Right Motor: ",h.GetDrivetrain().rDrive.motor.getPower());
    }

    public static final class TelemetryManagerTask implements Runnable {
        private  Hardware h;
        private  Telemetry t;
        private  OpModeBase o;

        public TelemetryManagerTask(Hardware h, Telemetry t, OpModeBase op) {
            this.h = h;
            this.t = t;
            this.o = op;

        }

        @Override
        public void run() {
            if (!(this.o.opModeIsActive())) Thread.currentThread().interrupt();
            TelemetryManager.manageTelem(h, t);
        }
    }
}
